import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import {useContext} from 'react'
import UserContext from '../UserContext'
import Link from 'next/link'


export default function NavBar(){
	const { user } = useContext(UserContext)

	return(
			<Navbar bg='info' className='navbar navbar-fixed-top' expand='lg'>
				<Link href='/'>			
					<a className='navbar-brand'>Budget Tracker</a>
				</Link>					
				<Navbar.Toggle aria-controls='basic-navbar-nav' />
				<Navbar.Collapse bg='light'>			
				{
					user.id === null
					? 
					<Nav className='ml-auto'>
						<Link href='/register'>
							<a className='nav-link'>Register</a>
						</Link>
						<Link href='/login'>
							<a className='nav-link'>Login</a>
						</Link>
					</Nav>
					:
					<>
					<Nav className='mr-auto'>
						<Link href='/categories'>
							<a className='nav-link'>Categories</a>
						</Link>
						<Link href='/records'>
							<a className='nav-link'>Records</a>
						</Link>
						<Link href='/monthlyIncome'>
							<a className='nav-link'>Monthly Income</a>
						</Link>
						<Link href='/monthlyExpense'>
							<a className='nav-link'>Monthly Expenses</a>
						</Link>
					
					</Nav>
					<Nav className='ml-left'>
						<Link href='/logout'>
							<a className='nav-link'>Logout</a>
						</Link>
					</Nav>
					</>
				}
				</Navbar.Collapse>
			</Navbar>
		)
}