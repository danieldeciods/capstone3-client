import Button from 'react-bootstrap/Button'
import {useContext} from 'react'
import UserContext from '../UserContext'
import { Row, Col } from 'react-bootstrap'
import Jumbotron from 'react-bootstrap/Jumbotron'
 


export default function Home(){
	const { user } = useContext(UserContext)
    return(
    	<>
    		<Jumbotron className='w-50 mx-auto mt-5 jumbotron text-center fluid container border border-info border-radius p-4'>    			
		    	<h1 className='m-0 p-1'>Budget Tracker</h1>
				<h5 className='m-0 p-1 mb-5'>Manage your Budget with Ease</h5> 
		    	{
		    		user.id === null
		    		? 	<Button href='/login' variant='info' block className='w-50 mx-auto'>Login</Button>
		    		: 	<>
		    			<Button href='/categories' variant='info' block className='mb-3 w-50 mx-auto'>Categories</Button>
		    			<Button href='/records' variant='info' block className='w-50 mx-auto'>Records</Button>
		    			</>
		    	}      			
    		</Jumbotron>	    	
	    </>
      )
}