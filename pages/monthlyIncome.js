import { useState, useEffect } from 'react'
import MonthlyIncome from '../components/MonthlyIncome'
import Head from 'next/head'
import { Row } from 'react-bootstrap'



export default function monthlyIncome(){

	const [ data, setData ] = useState([])

	useEffect(()=>{
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
		})
		.then(res => res.json())
		.then(data => {
			setData(data.records)
		})
	}, [])


	const incomeData = data.filter(income => {
		if(income.categoryType === 'income'){
			return income
		} else {
			return null
		}
	})


	return(
		<>
		<Row className='bg-light p-1 m-5'>
			<h1>Monthly Income in PHP</h1>
			{data.length > 0 ? <MonthlyIncome rawData={incomeData} /> : null}
		</Row>
		</>
		)

}