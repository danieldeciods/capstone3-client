import { useState, useEffect } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { Row, Col } from 'react-bootstrap'
import moment from 'moment'


export default function records(){

	const [ records, setRecords ] = useState([])
	const [ searchType, setSearchType ] = useState('All')
	const [ searchBar, setSearchBar ] = useState('')
	let filteredRecords = []


	useEffect(()=>{
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/records`,{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {			
			if(searchType === 'All'){
				if(searchBar === ''){
					setRecords(data.map(record => {						
						return(
							<Row key={record._id} className='border py-3 bg-light w-75 mx-auto bg-light border border-info'>
								<Col>
									<span>
										<h4>{record.description}</h4>
										<h6>{record.categoryType} ({record.categoryName})</h6>
										<h6>{moment(record.date).format('MMM Do YYYY')}</h6>
									</span>
								</Col>
								{
									record.categoryType === 'income'
									?	<Col className='text-right incomeRec'>						
											<h6 className='text-success'>+ {record.amount}</h6>
											<h6>{record.currentBalance}</h6>						
									 	</Col>
									:	<Col className='text-right expenseRec'>						
											<h6 className='text-danger'>- {record.amount}</h6>
											<h6>{record.currentBalance}</h6>						
								 		</Col>
								}
							</Row>
							)
					}))				
				} else {					
					setRecords(data.map(record => {					
						let recordDescription = record.description
						if(recordDescription.toLowerCase().match(searchBar.trim())){
							return(
								<Row key={record._id} className='border py-3 bg-light w-75 mx-auto bg-light border border-info'>
									<Col>
										<span>
											<h4>{record.description}</h4>
											<h6>{record.categoryType} ({record.categoryName})</h6>
											<h6>{moment(record.date).format('MMM Do YYYY')}</h6>
										</span>
									</Col>
									{
										record.categoryType === 'Income'
										?	<Col className-='text-right incomeRec'>
												<h6 className='text-success'>+ {record.amount}</h6>
												<h6>{record.currentBalance}</h6>
									 		</Col>
										:	<Col className='text-right expenseRec'>
												<h6 className='text-danger'>- {record.amount}</h6>
												<h6>{record.currentBalance}</h6>
								 			</Col>
									}
								</Row>
								)
						}
					}))
				}
			} else if(searchType === 'Income'){				
				setRecords(data.map(record => {
					if(record.categoryType === 'income'){						
						let recordDescription = record.description
						if(recordDescription.toLowerCase().match(searchBar.trim())){
							return (
								<Row key={record._id} className='border py-3 bg-light w-75 mx-auto bg-light border border-info'>
									<Col>
										<span>
											<h4>{record.description}</h4>
											<h6>{record.categoryType} ({record.categoryName})</h6>
											<h6>{moment(record.date).format('MMM Do YYYY')}</h6>
										</span>
									</Col>								
									<Col className='text-right incomeRec'>
										<h6 className='text-success'>+ {record.amount}</h6>
										<h6>{record.currentBalance}</h6>
									</Col>								
								</Row>
								)
						}
					}
				}))
			} else {
				setRecords(data.map(record => {
					if(record.categoryType === 'expense'){
						let recordDescription = record.description
						if(recordDescription.toLowerCase().match(searchBar.trim())){
							return(
								<Row key={record._id} className='border py-3 bg-light w-75 mx-auto bg-light border border-info'>
									<Col>
										<span>
											<h4>{record.description}</h4>
											<h6>{record.categoryType} ({record.categoryName})</h6>
											<h6>{moment(record.date).format('MMM Do YYYY')}</h6>
										</span>
									</Col>								
									<Col className='text-right expenseRec'>
										<h6 className='text-danger'>- {record.amount}</h6>
										<h6>{record.currentBalance}</h6>
									</Col>								
								</Row>
								)
						}
					}
				}))
			}
		})	
	}, [searchBar, searchType])
	

	return(
		<>
		<h1 className='m-4'>Records</h1>
		<Row className='m-2 w-50 mx-auto'>	
			<Col xs={4}>
				<Button href='/newRecords' className='mx-auto border text-center' block variant='info'>Add</Button>
			</Col>
			<Col xs={4}>	
				<Form className='mx-auto'>
					<Form.Control type='text' placeholder='Search Record' value={searchBar} onChange={(e) => setSearchBar(e.target.value)} required/>
				</Form>
			</Col>				
			<Col xs={4}>			
				<Form className='mx-auto'>
					<Form.Control as='select' placeholder='all' value={searchType} onChange={(e) => setSearchType(e.target.value)} required>
						<option>All</option>
						<option>Income</option>
						<option>Expense</option>
					</Form.Control>
				</Form>			
			</Col>					
		</Row>			
		{records}	
		<br />	
		</>
		)
}