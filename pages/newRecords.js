import { useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import Router from 'next/router'


export default function newRecords(){

	const [ type, setType ] = useState('select')
	const [ name, setName ] = useState('select')
	const [ amount, setAmount] = useState('')
	const [ description, setDescription ] = useState('')
	const [ catArr, setCatArr ] =useState([])
	const [ balance, setBalance ] =useState(0)
	const [ isActive, setIsActive ] =useState(false)
	const [ date, setDate ] = useState('')
	

	useEffect(()=>{
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				setBalance(data.balance)
				setCatArr(data.category)
			}
		})
		if(name !== '' && type !== '' && name !== 'select' && type !== 'select' && amount !== 0 && description !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, type])	
	

	function newRec(e){
		e.preventDefault()
		if(type === 'income'){	
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/addRecord`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					categoryName: name,
					categoryType: type,
					amount: amount,
					description: description,
					balance: parseInt(amount) + parseInt(balance),
					currentBalance: parseInt(amount) + parseInt(balance),
					date: date
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data){
					Swal.fire({
						icon: 'success',
						title: 'Successfully Created New Record'
					})
					setName('')
					setType('')
					setAmount('')
					setDescription('')
					Router.push('/records')
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Something is Wrong'
					})
				}
			})
		} else {
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/addRecord`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					categoryName: name,
					categoryType: type,
					amount: amount,
					description: description,
					balance: parseInt(balance) - parseInt(amount),
					currentBalance: parseInt(balance) - parseInt(amount),
					date: date
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data){
					Swal.fire({
						icon: 'success',
						title: 'Successfully Created New Record'
					})
					setName('')
					setType('')
					setAmount('')
					setDescription('')
					setDate('')
					Router.push('/records')
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Something is Wrong'
					})
				}
			})
		}
	}


	const catName = catArr.map(category => {
		if(type == 'income'){
			if(category.categoryType == 'income'){
			return(
				<option value={category.categoryName}>{category.categoryName}</option>
				)
			}			
		} else {
			if(category.categoryType == 'expenses'){
			return(
				<option value={category.categoryName}>{category.categoryName}</option>
				)
			}
		}
	})


	return(
		<>
			<h1 className='my-4 d-flex justify-content-center'>New Records</h1>
			<Form className='mx-auto w-50 bg-light border border-info p-4 m-5' onSubmit={(e) => newRec(e)}>
				<Form.Group>
					<Form.Label>Category Type:</Form.Label>
					<Form.Control as='select' placeholder='Enter Category Type' value={type} onChange={(e) => setType(e.target.value)} required>
						<option value={"select"}>Select</option>
						<option value={"income"}>Income</option>
						<option value={"expense"}>Expense</option>
					</Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Category Name:</Form.Label>
					<Form.Control as='select' placeholder='Enter Category Name' value={name} onChange={(e) => setName(e.target.value)} required>
						<option value={"select"}>Select</option>
						{catName}						
					</Form.Control>					
				</Form.Group>
				<Form.Group>
					<Form.Label>Amount:</Form.Label>
					<Form.Control type='number' placeholder='0' value={amount} onChange={(e) => setAmount(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type='text' placeholder='Enter Description' value={description} onChange={(e) => setDescription(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Date:</Form.Label>
					<Form.Control type='date' placeholder='Enter Date' value={date} onChange={(e) => setDate(e.target.value)} required/>
				</Form.Group>
				{
					isActive 
					? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
					: <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>	
				}
			</Form>
		</>
		)
}


