import { useState, useEffect } from 'react'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import { Row, Col } from 'react-bootstrap'
 

export default function categories(){

	const [ categories, setCategories ] = useState([])


	useEffect(()=>{
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {			
			if(data){
				setCategories(data.category)
			} else {
				setCategories([])				
			}			
		})
	}, [])


	const categoryData = categories.map(category => {
		return(
			<tr key={category._id}>
				<td>{category.categoryName}</td>
				<td>{category.categoryType}</td>
			</tr>
			)
	})


	return( 
		<>
		<h1 className='my-4'>Categories</h1>
		<Row className='w-75 mx-auto'>
 			<Col className='d-flex justify-content-center my-2'>
 				<Button href='/newCategory' variant='info' className='w-25 border'>Add</Button>
 			</Col>
		</Row>	
		<Row className='mb-5 w-75 mx-auto'>
			<Col>
				<Table striped bordered hover responsive className='bg-light mx-auto p-0'>
					<thead className='text-center border'>
						<tr>
							<th>Category</th>
							<th>Type</th>
						</tr>
					</thead>
					<tbody>
						{categoryData}				
					</tbody>
				</Table>
			</Col>
		</Row>
		</>
		)
}